require 'rails_helper'

RSpec.describe InputFile, type: :model do
  describe '#self.where' do
    it 'should raise an exception when attributes have an invalid key' do
      expect { InputFile.where(foo: 'bar') }.to raise_exception('Invalid key for where. Valid keys are [:username, :inbox, :size]')
    end

    it 'should return all elements when there are no attributes' do
      expect(InputFile.where.size).to eq(20004)
    end

    it 'should return a set of elements when attributes are valid' do
      expect(InputFile.where(username: 'difedaliri@uol.com.br')).to eq([%w(difedaliri@uol.com.br inbox 002301350 size 002001218)])
    end

    it 'should return an empty array when no elements are found' do
      expect(InputFile.where(username: 'aaaa')).to eq([])
    end
  end

  describe '#self.all' do
    it 'should return all elements when there are no attributes' do
      expect(InputFile.where.size).to eq(20004)
    end
  end

  describe '#self.attributes' do
    it 'should return a hash with avaliable attributes and their index' do
      expect(InputFile.attributes).to eq({ username: 0, inbox: 2, size: 4 })
    end
  end
end
