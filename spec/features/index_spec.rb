require 'rails_helper'

describe "the index listing", type: :feature do
  it "show me a page with all paginated data" do
    visit '/'
    expect(page).to have_content 'damejoxo@uol.com.br'
    expect(page).to have_content '002200463'
    expect(page).to have_content '002142222'
    expect(page).to_not have_content 'unnyfecepri@uol.com.br'
    expect(page).to_not have_content '010115531'
    expect(page).to_not have_content '001032547'
  end

  it "can navigate through pages" do
    visit '/'
    click_on 'Next'
    expect(page).to_not have_content 'damejoxo@uol.com.br'
    expect(page).to_not have_content '002200463'
    expect(page).to_not have_content '002142222'
    expect(page).to have_content 'unnyfecepri@uol.com.br'
    expect(page).to have_content '010115531'
    expect(page).to have_content '001032547'
  end

  it "filter by username" do
    visit '/'
    fill_in('Username', with: 'guvabelme@uol.com.br')
    click_on 'Filtrar'
    expect(page).to have_content 'guvabelme@uol.com.br'
    expect(page).to have_content '011110662'
    expect(page).to have_content '012114310'
  end

  it "filter by inbox" do
    visit '/'
    fill_in('Inbox', with: '011110662')
    click_on 'Filtrar'
    expect(page).to have_content 'guvabelme@uol.com.br'
    expect(page).to have_content '011110662'
    expect(page).to have_content '012114310'
  end

  it "filter by inbox" do
    visit '/'
    fill_in('Size', with: '012114310')
    click_on 'Filtrar'
    expect(page).to have_content 'guvabelme@uol.com.br'
    expect(page).to have_content '011110662'
    expect(page).to have_content '012114310'
  end

  it 'dont find with doesnt exists' do
    visit '/'
    fill_in('Size', with: '9999999')
    click_on 'Filtrar'
    expect(page).to_not have_content '9999999'
  end
end
