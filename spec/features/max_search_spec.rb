require 'rails_helper'

describe "the max value search", type: :feature do
  it "show me a page only with search form" do
    visit '/max'
    expect(page).to_not have_content 'A linha que contém o maior valor para username é:'
    expect(page).to_not have_content 'A linha que contém o maior valor para inbox é:'
    expect(page).to_not have_content 'A linha que contém o maior valor para size é:'
  end

  it "show me a page with the max value for the username when I choose it" do
    visit '/max'
    select('username', from: 'attribute')
    click_on 'Buscar'
    expect(page).to have_content 'A linha que contém o maior valor para username é:'
    expect(page).to_not have_content 'A linha que contém o maior valor para inbox é:'
    expect(page).to_not have_content 'A linha que contém o maior valor para size é:'
    expect(page).to have_content 'Username: zzvenaju@uol.com.br'
    expect(page).to have_content 'Inbox: 001004028'
    expect(page).to have_content 'Size: 000105267'
  end

  it "show me a page with the max value for the inbox when I choose it" do
    visit '/max'
    select('inbox', from: 'attribute')
    click_on 'Buscar'
    expect(page).to_not have_content 'A linha que contém o maior valor para username é:'
    expect(page).to have_content 'A linha que contém o maior valor para inbox é:'
    expect(page).to_not have_content 'A linha que contém o maior valor para size é:'
    expect(page).to have_content 'Username: jobenapad@uol.com.br'
    expect(page).to have_content 'Inbox: 012345678'
    expect(page).to have_content 'Size: 012135233'
  end

  it "show me a page with the max value for the size when I choose it" do
    visit '/max'
    select('size', from: 'attribute')
    click_on 'Buscar'
    expect(page).to_not have_content 'A linha que contém o maior valor para username é:'
    expect(page).to_not have_content 'A linha que contém o maior valor para inbox é:'
    expect(page).to have_content 'A linha que contém o maior valor para size é:'
    expect(page).to have_content 'Username: juvati_be@uol.com.br'
    expect(page).to have_content 'Inbox: 000232478'
    expect(page).to have_content 'Size: 012345671'
  end
end
