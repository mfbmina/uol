require 'rails_helper'

RSpec.describe DataController, type: :controller do
  describe 'GET max' do
    it 'should initialize @max with an empty array when params doesnt have attribute key' do
      get :max
      expect(assigns(:max)).to eq([])
      expect(response).to have_http_status(:success)
    end

    it 'should initialize @max with a array when params doesnt have attribute key' do
      get :max, params: { attribute: 'username' }
      expect(assigns(:max)).to eq(["zzvenaju@uol.com.br", "inbox", "001004028", "size", "000105267"])
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET max' do
    it 'should initialize @data with an  array' do
      get :index
      expect(assigns(:data)).to be_an_instance_of(Kaminari::PaginatableArray)
      expect(response).to have_http_status(:success)
    end

    it 'should filter @data' do
      get :index, params: { username: 'zzvenaju@uol.com.br' }
      expect(assigns(:data)).to eq([["zzvenaju@uol.com.br", "inbox", "001004028", "size", "000105267"]])
      expect(response).to have_http_status(:success)
    end
  end
end
