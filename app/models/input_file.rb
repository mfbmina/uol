module InputFile
  FILE_PATH = "#{Rails.root}/input.txt".freeze
  ATTRIBUTES = { username: 0, inbox: 2, size: 4 }

  def self.attributes
    ATTRIBUTES
  end

  def self.max(attribute)
    return [] if attribute.blank?
    where.max do |value, next_value|
      begin
        index = ATTRIBUTES[attribute.to_sym]
        value[index] <=> next_value[index]
      rescue StandardError => e
        raise_attribute_name
      end
    end
  end

  def self.where(hash = {})
    response = []
    file = File.open(FILE_PATH, 'r') do |f|
      f.each_line do |line|
        array_line = line.split(' ')
        response << array_line
        hash.each_pair do |k, v|
          next if v.blank?
          begin
            index = ATTRIBUTES[k]
            response.delete(array_line) if array_line[index] != v
          rescue StandardError => e
            raise_attribute_name
          end
        end
      end
    end
    response
  end

  private

  def self.raise_attribute_name
    raise "Invalid key for where. Valid keys are #{ATTRIBUTES.keys}"
  end
end
