class DataController < ApplicationController
  def index
    @data = Kaminari.paginate_array(InputFile.where(search_params)).page(params[:page]).per(10)
  end

  def max
    @max = InputFile.max(params[:attribute])
  end

  private

  def search_params
    {
      username: params[:username],
      inbox: params[:inbox],
      size: params[:size]
    }
  end
end
